import 'package:flutter/material.dart';
import 'package:flutter_crud/data/dummy_users.dart';
import 'package:flutter_crud/models/use.dart';
import 'package:flutter_crud/routes/app_routes.dart';
import 'package:flutter_crud/views.dart/user_form.dart';
import 'package:flutter_crud/widgets.dart/user_tile.dart';
import 'package:flutter_crud/widgets/user_tile.dart';
import 'package:provider/provider.dart';

import '../provider/user.dart';

class UserList extends StatelessWidget {
  //const UserList(User elementAt, {super.key, required String title});

  @override
  Widget build(BuildContext context) {
    final Users users = Provider.of(context);
    return Scaffold(
        appBar: AppBar(title: Text('Lista de Usuários'), actions: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                Navigator.of(context).pushNamed(AppRoutes.user_form);
               
              },
            ),
          )
        ]),
        body: ListView.builder(
            itemCount: users.count,
            itemBuilder: (context, index) => UserTile(
                  users.byIndex(index),
                  title: '',
                )));
  }
}
