import 'package:flutter/material.dart';
import 'package:flutter_crud/models/use.dart';
import 'package:flutter_crud/provider/user.dart';
import 'package:provider/provider.dart';

class UserForm extends StatelessWidget {
   UserForm({super.key});

  final _form = GlobalKey<FormState>();
  final Map<String, String> _formData = {};


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Formulário de Usuário'),
        actions: <Widget>[
          IconButton(onPressed: () {
            final isValid = _form.currentState!.validate();

            if(isValid){
              _form.currentState!.save();
              
              Provider.of<Users>(context, listen: false).put(User(
                id: _formData ['id']!,
                name: _formData['name']!,
                email: _formData['email']!,
                avatarURL: _formData['avatarUrl']!,
              ));
              Navigator.of(context).pop();
            }
             
          }, icon: Icon(Icons.save))
        ]
      ),
      body: Padding(padding: EdgeInsets.all(10),
      child: Form(
        key: _form,
        child: Column(
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: 'Nome'), validator: (value){
              if(value == null || value.trim().isEmpty){
                return ('Nome Inválido');
              }

              if(value.trim().length < 3){

                return ('Nome muito pequeno, no mínimo 3 letras.');

              }
              
            }, onSaved: (value) => _formData['name'] = value!,),
          TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            onSaved: (value) => _formData['Email'] = value!),
            TextFormField(
            decoration: InputDecoration(labelText: 'Url do avatar'),
            onSaved: (value) => _formData['AvatarUrl'] = value!)
        ],
      )),),

    );
  }
}