import 'package:flutter/material.dart';
import 'package:flutter_crud/models/use.dart';

import '../routes/app_routes.dart';

class UserTile extends StatelessWidget {
  final User user;
 
  const UserTile(this.user, {super.key, required String title});
 
  @override
  Widget build(BuildContext context) {
    final avatar = user.avatarURL == null || user.avatarURL.isEmpty
    ? CircleAvatar(child: Icon(Icons.person)):
    CircleAvatar(backgroundImage:NetworkImage(user.avatarURL));
    return  ListTile(
          leading: avatar,
          title: Text(user.name),
          subtitle: Text(user.email),
          trailing: Container(
            width: 100,
            child: Row( 
              children: <Widget>[
                IconButton(onPressed: () {

                }, 
                  icon: Icon(Icons.edit,
                  color: Colors.orange,)
              ),
                IconButton(onPressed: () {
                  Navigator.of(context).pushNamed(AppRoutes.user_form,
                  arguments: user
                  );

                }, 
                  icon: Icon(Icons.delete,
                  color: Colors.red,)
              )
            ],),
          ),
        );
  }
}
