import '../models/use.dart';

const DUMMY_USERS = {

'1': const User(
  id: '1',
  name: 'Nicácio',
  email: 'fredmed@gmail.com',
  avatarURL: 'https://cdn.pixabay.com/photo/2015/07/20/12/53/gehlert-852762_1280.jpg'),
  '2': const User(
  id: '2',
  name: 'Cebola',
  email: 'cebolao13@gmail.com',
  avatarURL: 'https://cdn.pixabay.com/photo/2015/08/16/12/38/man-890885_960_720.jpg'),

'3': const User(
  id: '3',
  name: 'Chaves',
  email: 'chavkey@gmail.com',
  avatarURL: 'https://cdn.pixabay.com/photo/2020/01/25/21/55/adult-4793442_960_720.jpg'),
};
